Koenig & Owen, LLC is one of the top criminal defense law firms in Columbus, Ohio. Our attorneys have an outstanding track record of success in the Columbus criminal courts. Charles Koenig and Jim Owen have over 50 years of combined experience, during which they have handled some of the toughest cases in Ohio.

Jim Owen won his first murder case over 25 years ago. Ever since, he has been collecting not-guilty verdicts in all kinds of major criminal cases, including aggravated murder, bank robbery, kidnapping, attempted rape, gross sexual imposition, aggravated drug trafficking, and more. If youre facing similar offenses, hiring Jim Owen is one of the best things you can do to protect your future. 

Charles A. Koenig has a long career in the white collar crimes and business defense areas. With his background in finance, Chuck has been able to represent people and businesses facing tax charges, fraud, securities violations, insider trading, ERISA violations, and other financial charges. When it comes to tax or other financial charges, few people in the country have as much experience as Chuck does.

Dont just hire any law firm when it comes to your freedom. The attorneys at Koenig &amp; Owen, LLC have the results and the experience necessary to successfully represent you. If you need more proof, call us today so we can discuss your case with you. We offer a free, no-obligation consultation, so contact us now. 

Give us a call today to claim your free consultation. We will discuss your case with you without any obligations. 

Koenig & Owen, LLC
5354 N High St #101
Columbus, OH 43214
(614) 454-5010
https://www.columbuscriminaldefenseattorney.com